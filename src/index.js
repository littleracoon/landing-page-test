import Vue from 'vue';
import { NavbarPlugin, ModalPlugin } from 'bootstrap-vue';
import { TeamItem } from './templates/team';

import './components/carousel';
import './scss/main.scss';
import './scss/spritesheet.scss';

Vue.use(NavbarPlugin);
Vue.use(ModalPlugin);

const navigation = new Vue({
  el: '#navigation'
});

const team = new Vue({
  el: '#teams',
  components: {
    TeamItem
  }
});

const storyModal = new Vue({
  el: '#story',
  data: {
    modalShow: false
  }
});



